# TransferMoney

The application uses dropwizard framework version 2.0.0, H2 in-memory DB, liquibase and Hibernate.  
There is a suite of unit tests that test the application resources which will be run when maven command is run.  
For functional tests there is a chapter below that describes how to run them.  

How to start the TransferMoney application
---

1. Run `mvn clean install` to build and unit test the application
2. Run DB migration with `java -jar target/transfer-money-1.1.jar db migrate config.yml`
3. Start application with `java -jar target/transfer-money-1.1.jar server config.yml`

More information can be found [here](https://www.dropwizard.io/en/stable/manual/example.html).

Rest API exposed by the application
---

1. GET     /api/accounts
2. GET     /api/accounts/{id}
3. POST    /api/accounts
4. GET     /api/transactions
5. POST    /api/transfer

Resources definition
---

1. Accounts
`{
  "id": "534b3de0-74c1-11e8-adc0-fa7ae01bbebc",
  "name": "Account1",
  "balance": 100.00,
  "currency": "EUR",
  "state": "ACTIVE",
  "type": "REVOLUT"
}`  
   An account is identified by the id (uuid), name (text), balance (decimal), currency (3-letter ISO currency code), state (ACTIVE/INACTIVE) and type (REVOLUT/OTHER).
   
2. Transaction
`{
	"id": 123456789,
	"state": "Completed",
	"transferId": 1
}`
   A transaction is identified by the id (long), a state (COMPLETED/FAILED/DECLINED/STARTED) and a transferId.
   A transaction is marked with a state as follows:
    * COMPLETED: Revolut to Revolut transfer and Other to Revolut
    * STARTED: Revolut to Other (I assumed that this transfer is redirected to a 3rd party for processing)
    * FAILED: Invalid account or Inactive account
    * DECLINED: Not enough money to make the transfer or transfer from OTHER to OTHER  
	
3. Transfer
`{
  "transferId": 123456789,
  "sourceAccountId": "14633280-74ae-11e8-adc0-fa7ae01bbebc",
  "targetAccountId": "1463350a-74ae-11e8-adc0-fa7ae01bbebc",
  "amount": 80.27,
  "currency": "RON",
  "reference": "TV&Net"
}`  
A transfer is identified by the transferId (long), source and target account id's (uuid), amount (decimal), currency (3-letter ISO currency code) and reference (text).  

How to test the API
---

In order to test the API the application has to be started.
Then, requests can be made using curl tool, as follows:

1. Transfer money:
(run from functional tests directory): `curl -d "@transfer1.json" -H "Content-Type: application/json" -X POST http://localhost:8080/api/transfer`
which will return a response with a transaction id and the state of the transaction.
transfer1.json contains an example of a valid transfer between two Revolut accounts.  
There are other files in this directory that can be used for functional tests or new ones can be created.  

2. Get list of accounts
`curl -X GET http://localhost:8080/api/accounts`

3. Get a single account
`curl -X GET http://localhost:8080/api/accounts/{uuid}`

4. Get the list of transactions
`curl -X GET http://localhost:8080/api/transactions`

What data can be used for testing
---

The application will load into H2 database, at startup, 10 accounts that can be used for testing.
