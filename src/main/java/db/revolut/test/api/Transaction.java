package db.revolut.test.api;

import db.revolut.test.core.TransactionState;

import javax.persistence.*;
import java.util.Objects;

@SuppressWarnings("JpaQlInspection")
@Entity
@Table(name = "transactions")
@NamedQueries({
        @NamedQuery(name = "db.revolut.test.api.transaction.findAll",
                query = "SELECT t from Transaction t")
})
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "state", nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionState state;

    @Column(name = "transferId", nullable = false)
    private long transferId;

    public Transaction() {
    }

    public Transaction(long id, TransactionState state, long transferId) {
        this.id = id;
        this.state = state;
        this.transferId = transferId;
    }

    public Transaction(TransactionState state, long transferId) {
        this.state = state;
        this.transferId = transferId;
    }

    public long getId() {
        return id;
    }

    public TransactionState getState() {
        return state;
    }

    public long getTransferId() {
        return transferId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction that = (Transaction) o;
        return id == that.id &&
                transferId == that.transferId &&
                state == that.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, state, transferId);
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", state=" + state +
                ", transferId=" + transferId +
                '}';
    }
}
