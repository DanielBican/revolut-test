package db.revolut.test.api;

import db.revolut.test.core.CurrencyType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "transfers")
public class Transfer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "sourceAccountId", nullable = false)
    @NotNull
    private UUID sourceAccountId;

    @Column(name = "targetAccountId", nullable = false)
    @NotNull
    private UUID targetAccountId;

    @Column(name = "amount", nullable = false, precision = 15, scale = 2)
    @Positive
    @NotNull
    private BigDecimal amount;

    @Column(name = "currency", nullable = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    private CurrencyType currency;

    @Column(name = "reference")
    @NotNull
    private String reference;

    public Transfer() {
    }

    public Transfer(long id, UUID sourceAccountId, UUID targetAccountId,
                    BigDecimal amount, CurrencyType currency, String reference) {
        this.id = id;
        this.sourceAccountId = sourceAccountId;
        this.targetAccountId = targetAccountId;
        this.amount = amount;
        this.currency = currency;
        this.reference = reference;
    }

    public Transfer(UUID sourceAccountId, UUID targetAccountId,
                    BigDecimal amount, CurrencyType currency, String reference) {
        this.sourceAccountId = sourceAccountId;
        this.targetAccountId = targetAccountId;
        this.amount = amount;
        this.currency = currency;
        this.reference = reference;
    }

    public long getId() {
        return id;
    }

    public UUID getSourceAccountId() {
        return sourceAccountId;
    }

    public UUID getTargetAccountId() {
        return targetAccountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public String getReference() {
        return reference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Transfer)) {
            return false;
        }

        Transfer t = (Transfer) o;

        return Objects.equals(id, t.id) &&
                Objects.equals(sourceAccountId, t.sourceAccountId) &&
                Objects.equals(targetAccountId, t.targetAccountId) &&
                Objects.equals(amount, t.amount) &&
                Objects.equals(currency, t.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sourceAccountId, targetAccountId, amount, currency, reference);
    }

    @Override
    public String toString() {

        return "Transfer id: " +
                id +
                "\n" +
                "Source account id: " +
                sourceAccountId +
                "\n" +
                "Target account id: " +
                targetAccountId +
                "\n" +
                "Amount: " +
                amount +
                "\n" +
                "Currency: " +
                currency +
                "\n" +
                "Reference: " +
                reference +
                "\n";
    }
}
