package db.revolut.test.api;

import db.revolut.test.core.AccountState;
import db.revolut.test.core.AccountType;
import db.revolut.test.core.CurrencyType;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

@SuppressWarnings("JpaQlInspection")
@Entity
@Table(name = "accounts")
@NamedQueries(
        {
                @NamedQuery(name = "db.revolut.test.api.Account.findAll", query = "SELECT a FROM Account a"),
                @NamedQuery(name = "db.revolut.test.api.Account.findByName", query = "SELECT a FROM Account a WHERE a.name = :name")
        }
)
public class Account {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "name", nullable = false)
    @NotNull
    @NotEmpty
    private String name;

    @Column(name = "balance", nullable = false, precision = 15, scale = 2)
    @Positive
    @NotNull
    private BigDecimal balance;

    @Column(name = "currency", nullable = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    private CurrencyType currency;

    @Column(name = "state")
    @NotNull
    @Enumerated(EnumType.STRING)
    private AccountState state;

    @Column(name = "type", nullable = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    private AccountType type;

    public Account() {
    }

    public Account(UUID id, String name, BigDecimal balance, CurrencyType currency, AccountState state, AccountType type) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.currency = currency;
        this.state = state;
        this.type = type;
    }

    public Account(String name, BigDecimal balance, CurrencyType currency, AccountState state, AccountType type) {
        this.name = name;
        this.balance = balance;
        this.currency = currency;
        this.state = state;
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public AccountState getState() {
        return state;
    }

    public AccountType getType() {
        return type;
    }

    public void deposit(BigDecimal amount) {
        balance = balance.add(amount);
    }

    public void withdraw(BigDecimal amount) {
        balance = balance.subtract(amount);
    }

    /**
     * Checks if the account will have a positive balance after the money are withdrawn.
     *
     * @param amount money to be withdrawn
     * @return true if the account has enough money.
     */
    public boolean withdrawAllowed(BigDecimal amount) {
        return balance.compareTo(amount) >= 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Account)) {
            return false;
        }

        Account a = (Account) o;

        return Objects.equals(id, a.id) &&
                Objects.equals(name, a.name) &&
                Objects.equals(balance, a.balance) &&
                Objects.equals(state, a.state) &&
                Objects.equals(currency, a.currency) &&
                Objects.equals(type, a.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, balance, state, currency, type);
    }

    @Override
    public String toString() {

        return "Account id: " +
                id +
                "\n" +
                "Name: " +
                name +
                "\n" +
                "Balance: " +
                balance +
                "\n" +
                "Currency: " +
                currency +
                "\n" +
                "State: " +
                state +
                "\n" +
                "Type: " +
                type +
                "\n";
    }
}
