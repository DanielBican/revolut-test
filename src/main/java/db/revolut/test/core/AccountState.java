package db.revolut.test.core;

public enum AccountState {
    ACTIVE,
    INACTIVE
}
