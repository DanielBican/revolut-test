package db.revolut.test.core;

public enum CurrencyType {
    EUR,
    USD,
    RON
}
