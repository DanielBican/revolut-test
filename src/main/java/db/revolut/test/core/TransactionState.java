package db.revolut.test.core;

public enum TransactionState {
    STARTED,
    COMPLETED,
    FAILED,
    DECLINED
}
