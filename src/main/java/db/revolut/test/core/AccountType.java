package db.revolut.test.core;

public enum AccountType {
    REVOLUT,
    OTHER
}
