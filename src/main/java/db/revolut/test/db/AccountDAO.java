package db.revolut.test.db;

import db.revolut.test.api.Account;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;
import java.util.UUID;

public class AccountDAO extends AbstractDAO<Account> {

    public AccountDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Account findById(UUID id) {
        return currentSession().get(Account.class, id, LockMode.PESSIMISTIC_WRITE);
    }

    @SuppressWarnings("unchecked")
    public List<Account> findAll() {
        return list((Query<Account>) namedQuery("db.revolut.test.api.Account.findAll"));
    }

    public Account findByName(String name) {
        return (Account) namedQuery("db.revolut.test.api.Account.findByName")
                .setParameter("name", name).getSingleResult();
    }

    public Account save(Account account) {
        return persist(account);
    }
}
