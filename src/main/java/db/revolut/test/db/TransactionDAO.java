package db.revolut.test.db;

import db.revolut.test.api.Transaction;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

public class TransactionDAO extends AbstractDAO<Transaction> {

    public TransactionDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Transaction findById(long id) {
        return get(id);
    }

    @SuppressWarnings("unchecked")
    public List<Transaction> findAll() {
        return list((Query<Transaction>) namedQuery("db.revolut.test.api.transaction.findAll"));
    }

    public Transaction save(Transaction t) {
        return persist(t);
    }
}
