package db.revolut.test.db;

import db.revolut.test.api.Transfer;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

public class TransferDAO extends AbstractDAO<Transfer> {

    public TransferDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Transfer save(Transfer t) {
        return persist(t);
    }
}
