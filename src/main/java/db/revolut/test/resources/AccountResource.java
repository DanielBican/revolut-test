package db.revolut.test.resources;

import db.revolut.test.api.Account;
import db.revolut.test.db.AccountDAO;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.UUIDParam;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    private final AccountDAO accountDAO;

    public AccountResource(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public List<Account> getAccounts() {
        return accountDAO.findAll();
    }

    @GET
    @Path("{id}")
    @UnitOfWork
    public Account getAccount(@PathParam("id") UUIDParam id) {
        Account account = accountDAO.findById(id.get());

        if (account == null) {
            throw new NotFoundException("No such account");
        }

        return account;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Account createAccount(@NotNull @Valid Account account) {
        return accountDAO.save(account);
    }
}
