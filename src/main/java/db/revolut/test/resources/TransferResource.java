package db.revolut.test.resources;

import db.revolut.test.api.Account;
import db.revolut.test.api.Transaction;
import db.revolut.test.api.Transfer;
import db.revolut.test.core.AccountState;
import db.revolut.test.core.AccountType;
import db.revolut.test.core.TransactionState;
import db.revolut.test.db.AccountDAO;
import db.revolut.test.db.TransactionDAO;
import db.revolut.test.db.TransferDAO;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("transfers")
public class TransferResource {

    private final static Logger logger = LoggerFactory.getLogger(TransferResource.class);

    private final AccountDAO accountDAO;

    private final TransferDAO transferDAO;

    private final TransactionDAO transactionDAO;

    public TransferResource(AccountDAO accountDAO, TransferDAO transferDAO, TransactionDAO transactionDAO) {
        this.accountDAO = accountDAO;
        this.transferDAO = transferDAO;
        this.transactionDAO = transactionDAO;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response transferMoney(@NotNull @Valid Transfer transfer, @Context UriInfo uriInfo) {

        Account sourceAccount = accountDAO.findById(transfer.getSourceAccountId());
        Account targetAccount = accountDAO.findById(transfer.getTargetAccountId());

        TransactionState transactionState;
        if ((sourceAccount == null) ||
                (targetAccount == null) ||
                (AccountState.INACTIVE == sourceAccount.getState()) ||
                (AccountState.INACTIVE == targetAccount.getState())) {
            transactionState = TransactionState.FAILED;
        } else if (!sourceAccount.withdrawAllowed(transfer.getAmount()) ||
                AccountType.OTHER == sourceAccount.getType() && AccountType.OTHER == targetAccount.getType()) {
            transactionState = TransactionState.DECLINED;
        } else if (AccountType.OTHER == targetAccount.getType()) {
            // Here we should send the transfer to a 3rd party.
            sourceAccount.withdraw(transfer.getAmount());
            transactionState = TransactionState.STARTED;
        } else if (AccountType.OTHER == sourceAccount.getType()) {
            // The source account should be debited by other party.
            targetAccount.deposit(transfer.getAmount());
            transactionState = TransactionState.COMPLETED;
        } else {
            sourceAccount.withdraw(transfer.getAmount());
            targetAccount.deposit(transfer.getAmount());
            transactionState = TransactionState.COMPLETED;
        }

        Transfer savedTransfer = transferDAO.save(transfer);
        Transaction transaction = transactionDAO.save(new Transaction(transactionState, savedTransfer.getId()));

        logger.debug("Source account: {} and target account: {}", sourceAccount, targetAccount);
        logger.debug("Transfer: {} generated transaction: {}", transfer, transaction);

        return Response.created(uriInfo.getAbsolutePathBuilder().path(String.valueOf(transfer.getId())).build())
                .entity(transaction)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}
