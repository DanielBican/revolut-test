package db.revolut.test.resources;

import db.revolut.test.api.Transaction;
import db.revolut.test.db.TransactionDAO;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("transactions")
public class TransactionResource {

    private final TransactionDAO transactionDAO;

    public TransactionResource(TransactionDAO transactionDAO) {
        this.transactionDAO = transactionDAO;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public List<Transaction> getTransactions() {
        return transactionDAO.findAll();
    }
}
