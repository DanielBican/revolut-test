package db.revolut.test;

import db.revolut.test.api.Account;
import db.revolut.test.api.Transaction;
import db.revolut.test.api.Transfer;
import db.revolut.test.db.AccountDAO;
import db.revolut.test.db.TransactionDAO;
import db.revolut.test.db.TransferDAO;
import db.revolut.test.resources.AccountResource;
import db.revolut.test.resources.TransactionResource;
import db.revolut.test.resources.TransferResource;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class TransferMoneyApplication extends Application<TransferMoneyConfiguration> {

    public static void main(final String[] args) throws Exception {
        new TransferMoneyApplication().run(args);
    }

    @Override
    public String getName() {
        return "TransferMoney";
    }

    private final HibernateBundle<TransferMoneyConfiguration> hibernate = new HibernateBundle<TransferMoneyConfiguration>(Account.class, Transaction.class, Transfer.class) {
        @Override
        public DataSourceFactory getDataSourceFactory(TransferMoneyConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    @Override
    public void initialize(Bootstrap<TransferMoneyConfiguration> bootstrap) {
        // Enable variable substitution with environment variables
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );
        bootstrap.addBundle(new MigrationsBundle<TransferMoneyConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(TransferMoneyConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(hibernate);
    }

    @Override
    public void run(final TransferMoneyConfiguration configuration,
                    final Environment environment) {
        final AccountDAO accountDAO = new AccountDAO(hibernate.getSessionFactory());
        final TransactionDAO transactionDAO = new TransactionDAO(hibernate.getSessionFactory());
        final TransferDAO transferDAO = new TransferDAO(hibernate.getSessionFactory());

        environment.jersey().register(new TransferResource(accountDAO, transferDAO, transactionDAO));
        environment.jersey().register(new TransactionResource(transactionDAO));
        environment.jersey().register(new AccountResource(accountDAO));
    }

}
