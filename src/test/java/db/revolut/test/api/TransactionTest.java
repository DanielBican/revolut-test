package db.revolut.test.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import db.revolut.test.core.TransactionState;
import io.dropwizard.jackson.Jackson;
import org.junit.jupiter.api.Test;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransactionTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializeToJSON() throws Exception {
        final Transaction transaction = new Transaction(123456789, TransactionState.COMPLETED,0);

        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/transaction.json"), Transaction.class));

        assertThat(MAPPER.writeValueAsString(transaction)).isEqualTo(expected);
    }

    @Test
    public void deserializeFromJSON() throws Exception {
        final Transaction transaction = new Transaction(123456789, TransactionState.COMPLETED, 0);

        assertThat(MAPPER.readValue(fixture("fixtures/transaction.json"), Transaction.class))
                .isEqualTo(transaction);
    }

    @Test
    public void testEquals() {
        final Transaction transaction1 = new Transaction(123456789, TransactionState.COMPLETED, 0);

        final Transaction transaction2 = new Transaction(123456789, TransactionState.COMPLETED, 0);

        assertEquals(transaction1, transaction2);
    }
}
