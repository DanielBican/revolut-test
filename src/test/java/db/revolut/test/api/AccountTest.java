package db.revolut.test.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import db.revolut.test.core.AccountState;
import db.revolut.test.core.AccountType;
import db.revolut.test.core.CurrencyType;
import io.dropwizard.jackson.Jackson;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializeToJSON() throws Exception {
        final Account transaction = new Account(UUID.fromString("534b3de0-74c1-11e8-adc0-fa7ae01bbebc"), "Account1",
                new BigDecimal("100.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT);

        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/account.json"), Account.class));

        assertEquals(expected, MAPPER.writeValueAsString(transaction));
    }

    @Test
    public void deserializeFromJSON() throws Exception {
        final Account transaction = new Account(UUID.fromString("534b3de0-74c1-11e8-adc0-fa7ae01bbebc"), "Account1",
                new BigDecimal("100.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT);

        assertEquals(transaction, MAPPER.readValue(fixture("fixtures/account.json"), Account.class));
    }

    @Test
    public void testEquals() {
        final Account account1 = new Account(UUID.fromString("534b3de0-74c1-11e8-adc0-fa7ae01bbebc"), "Account1",
                new BigDecimal("100.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT);

        final Account account2 = new Account(UUID.fromString("534b3de0-74c1-11e8-adc0-fa7ae01bbebc"), "Account1",
                new BigDecimal("100.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT);

        assertEquals(account1, account2);
    }
}
