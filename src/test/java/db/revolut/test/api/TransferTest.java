package db.revolut.test.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import db.revolut.test.core.CurrencyType;
import io.dropwizard.jackson.Jackson;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransferTest {

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializeToJSON() throws Exception {
        final Transfer transfer = new Transfer(123456789,
                UUID.fromString("14633280-74ae-11e8-adc0-fa7ae01bbebc"),
                UUID.fromString("1463350a-74ae-11e8-adc0-fa7ae01bbebc"),
                new BigDecimal("80.27"),
                CurrencyType.RON,
                "TV&Net");

        final String expected = MAPPER.writeValueAsString(
                MAPPER.readValue(fixture("fixtures/transfer.json"), Transfer.class));

        assertEquals(expected, MAPPER.writeValueAsString(transfer));
    }

    @Test
    public void deserializeFromJSON() throws Exception {
        final Transfer transfer = new Transfer(123456789,
                UUID.fromString("14633280-74ae-11e8-adc0-fa7ae01bbebc"),
                UUID.fromString("1463350a-74ae-11e8-adc0-fa7ae01bbebc"),
                new BigDecimal("80.27"),
                CurrencyType.RON,
                "TV&Net");

        assertEquals(transfer, MAPPER.readValue(fixture("fixtures/transfer.json"), Transfer.class));
    }

    @Test
    public void testEquals() {
        final Transfer transfer1 = new Transfer(123456789,
                UUID.fromString("14633280-74ae-11e8-adc0-fa7ae01bbebc"),
                UUID.fromString("1463350a-74ae-11e8-adc0-fa7ae01bbebc"),
                new BigDecimal("80.27"),
                CurrencyType.RON,
                "TV&Net");

        final Transfer transfer2 = new Transfer(123456789,
                UUID.fromString("14633280-74ae-11e8-adc0-fa7ae01bbebc"),
                UUID.fromString("1463350a-74ae-11e8-adc0-fa7ae01bbebc"),
                new BigDecimal("80.27"),
                CurrencyType.RON,
                "TV&Net");

        assertEquals(transfer1, transfer2);
    }
}
