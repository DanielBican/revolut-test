package db.revolut.test.resources;

import db.revolut.test.api.Account;
import db.revolut.test.api.Transaction;
import db.revolut.test.api.Transfer;
import db.revolut.test.core.AccountState;
import db.revolut.test.core.AccountType;
import db.revolut.test.core.CurrencyType;
import db.revolut.test.core.TransactionState;
import db.revolut.test.db.AccountDAO;
import db.revolut.test.db.TransactionDAO;
import db.revolut.test.db.TransferDAO;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(DropwizardExtensionsSupport.class)
public class TransferResourceTest {

    private static final AccountDAO accountDAO = mock(AccountDAO.class);
    private static final TransactionDAO transactionDAO = mock(TransactionDAO.class);
    private static final TransferDAO transferDAO = mock(TransferDAO.class);

    private static final ResourceExtension RULE = ResourceExtension.builder()
            .addResource(new TransferResource(accountDAO, transferDAO, transactionDAO))
            .build();

    @AfterEach
    public void tearDown() {
        reset(accountDAO);
        reset(transferDAO);
        reset(transactionDAO);
    }

    @Test
    public void testCreateTransfer() {

        final Account account1 = new Account(UUID.randomUUID(), "Account1",
                new BigDecimal("100.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT);
        final Account account2 = new Account(UUID.randomUUID(), "Account2",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT);

        when(accountDAO.findById(account1.getId())).thenReturn(account1);
        when(accountDAO.findById(account2.getId())).thenReturn(account2);

        final Transfer transfer = new Transfer(123456789,
                account1.getId(),
                account2.getId(),
                new BigDecimal("44.00"),
                CurrencyType.RON,
                "Donation");

        final Transaction transaction = new Transaction(1, TransactionState.COMPLETED, 123456789);

        when(transferDAO.save(transfer)).thenReturn(transfer);
        when(transactionDAO.save(any(Transaction.class))).thenReturn(transaction);

        final Response response = RULE.target("transfers")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(transfer));

        assertEquals(Response.Status.CREATED, response.getStatusInfo());
        assertNotNull(response.getLocation());
        assertEquals(transaction.getTransferId(), transfer.getId());
        assertEquals(transaction, response.readEntity(Transaction.class));
    }

    @Test
    public void testNegativeAmount() {

        final Transfer transfer = new Transfer(123456789,
                UUID.randomUUID(),
                UUID.randomUUID(),
                new BigDecimal("44.00").negate(),
                CurrencyType.RON,
                "Donation");

        final Response response = RULE.target("transfers")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(transfer));

        assertEquals(422, response.getStatus());
    }

    @Test
    public void testNullAccountId() {

        final Transfer transfer = new Transfer(123456789,
                null,
                UUID.randomUUID(),
                new BigDecimal("44.00"),
                CurrencyType.RON,
                "Donation");

        final Response response = RULE.target("transfers")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(transfer));

        assertEquals(422, response.getStatus());
    }
}
