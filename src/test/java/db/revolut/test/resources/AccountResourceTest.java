package db.revolut.test.resources;

import db.revolut.test.api.Account;
import db.revolut.test.core.AccountState;
import db.revolut.test.core.AccountType;
import db.revolut.test.core.CurrencyType;
import db.revolut.test.db.AccountDAO;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.GenericType;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(DropwizardExtensionsSupport.class)
public class AccountResourceTest {

    public static final AccountDAO accountDAO = mock(AccountDAO.class);

    public static final ResourceExtension RULE = ResourceExtension.builder()
            .addResource(new AccountResource(accountDAO))
            .build();

    @AfterEach
    public void tearDown() {
        reset(accountDAO);
    }

    @Test
    public void testGetAllAccounts() {

        final Account account1 = new Account("Account1",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT);

        final Account account2 = new Account("Account2",
                new BigDecimal("100.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT);

        when(accountDAO.findAll()).thenReturn(Arrays.asList(account1, account2));

        final List<Account> response = RULE.target("accounts")
                .request().get(new GenericType<List<Account>>() {
                });

        assertThat(response.size()).isEqualTo(2);
    }

    @Test
    public void testGetInexistentAccounts() {

        UUID uuid = UUID.randomUUID();

        when(accountDAO.findById(uuid)).thenReturn(null);

        Assertions.assertThrows(NotFoundException.class, () -> RULE.target("accounts/" + uuid)
                .request().get(new GenericType<List<Account>>() {
                }));
    }
}
