package db.revolut.test.resources;

import db.revolut.test.api.Transaction;
import db.revolut.test.core.TransactionState;
import db.revolut.test.db.TransactionDAO;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.GenericType;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(DropwizardExtensionsSupport.class)
public class TransactionResourceTest {

    private static final TransactionDAO transactionDAO = mock(TransactionDAO.class);

    private static final ResourceExtension RULE = ResourceExtension.builder()
            .addResource(new TransactionResource(transactionDAO))
            .build();

    private static final List<Transaction> transactions = new ArrayList<>();

    static {
        transactions.add(new Transaction(TransactionState.COMPLETED, 0));
        transactions.add(new Transaction(TransactionState.DECLINED, 1));
        transactions.add(new Transaction(TransactionState.FAILED, 2));
        transactions.add(new Transaction(TransactionState.STARTED, 3));
    }

    @AfterEach
    public void tearDown() {
        reset(transactionDAO);
    }

    @Test
    public void testGetTransactions() {

        when(transactionDAO.findAll()).thenReturn(transactions);

        final List<Transaction> response = RULE.target("transactions")
                .request().get(new GenericType<List<Transaction>>() {
                });
        assertThat(response).containsAll(transactions);
    }
}
