package db.revolut.test;

import db.revolut.test.api.Account;
import db.revolut.test.api.Transaction;
import db.revolut.test.api.Transfer;
import db.revolut.test.core.AccountState;
import db.revolut.test.core.AccountType;
import db.revolut.test.core.CurrencyType;
import db.revolut.test.core.TransactionState;
import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@ExtendWith(DropwizardExtensionsSupport.class)
public class IntegrationTest {

    private static final UUID NON_EXISTENT_ACCOUNT_UUID = UUID.fromString("eeeeeeee-dddd-cccc-bbbb-aaaaaaaaaaaa");

    private static final String TMP_FILE = createTempFile();
    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("test-config.yml");

    public static final DropwizardAppExtension<TransferMoneyConfiguration> RULE = new DropwizardAppExtension<>(
            TransferMoneyApplication.class, CONFIG_PATH,
            ConfigOverride.config("database.url", "jdbc:h2:" + TMP_FILE));

    @BeforeAll
    public static void migrateDb() throws Exception {
        RULE.getApplication().run("db", "migrate", CONFIG_PATH);
    }

    private static String createTempFile() {
        try {
            return File.createTempFile("test", null).getAbsolutePath();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Test
    public void testSameTransferDifferentTransaction() {

        final Account sourceAccount = postAccount(new Account("Account1",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));

        final Account targetAccount = postAccount(new Account("Account2",
                new BigDecimal("100.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));

        Transfer transfer = new Transfer(
                sourceAccount.getId(),
                targetAccount.getId(),
                new BigDecimal("10.00"),
                CurrencyType.EUR,
                "Beers");
        Transaction transaction1 = postTransfer(transfer);
        Transaction transaction2 = postTransfer(transfer);

        assertNotEquals(transaction1.getId(), transaction2.getId());
        assertNotEquals(transaction1.getTransferId(), transaction2.getTransferId());
    }

    @Test
    public void transferRevToRevEnoughMoney() {

        final Account sourceAccount = postAccount(new Account("Account2",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));
        final Account targetAccount = postAccount(new Account("Account1",
                new BigDecimal("100.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));

        final Transfer transferRevToRevEnoughMoney = new Transfer(
                sourceAccount.getId(),
                targetAccount.getId(),
                new BigDecimal("100.00"),
                CurrencyType.EUR,
                "Mortgage");
        final Transaction response = postTransfer(transferRevToRevEnoughMoney);

        // Transaction is correct.
        assertEquals(TransactionState.COMPLETED, response.getState());

        Account sourceAccountAfter = getAccount(sourceAccount.getId());
        Account targetAccountAfter = getAccount(targetAccount.getId());
        // Balance is correct.
        assertEquals(sourceAccount.getBalance().subtract(transferRevToRevEnoughMoney.getAmount()), sourceAccountAfter.getBalance());
        assertEquals(targetAccount.getBalance().add(transferRevToRevEnoughMoney.getAmount()), targetAccountAfter.getBalance());
    }

    @Test
    public void transferRevToOtherEnoughMoney() {

        final Account sourceAccount = postAccount(new Account("Account2",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));
        final Account targetAccount = postAccount(new Account("Account9",
                new BigDecimal("100.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.OTHER));

        final Transfer transferRevToOtherEnoughMoney = new Transfer(
                sourceAccount.getId(),
                targetAccount.getId(),
                new BigDecimal("100.00"),
                CurrencyType.EUR,
                "Mortgage");
        final Transaction response = postTransfer(transferRevToOtherEnoughMoney);

        // Transaction is correct.
        assertEquals(TransactionState.STARTED, response.getState());

        Account sourceAccountAfter = getAccount(sourceAccount.getId());
        Account targetAccountAfter = getAccount(targetAccount.getId());
        // Balance is correct.
        assertEquals(sourceAccount.getBalance().subtract(transferRevToOtherEnoughMoney.getAmount()), sourceAccountAfter.getBalance());
        assertEquals(targetAccount.getBalance(), targetAccountAfter.getBalance());
    }

    @Test
    public void transferOtherToRev() {

        final Account sourceAccount = postAccount(new Account("Account6",
                new BigDecimal("1200.00"), CurrencyType.USD,
                AccountState.ACTIVE, AccountType.OTHER));
        final Account targetAccount = postAccount(new Account("Account1",
                new BigDecimal("100.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));

        final Transfer transferOtherToRev = new Transfer(
                sourceAccount.getId(),
                targetAccount.getId(),
                new BigDecimal("50.00"),
                CurrencyType.EUR,
                "Gas");
        final Transaction response = postTransfer(transferOtherToRev);

        // Transaction is correct.
        assertEquals(TransactionState.COMPLETED, response.getState());

        Account sourceAccountAfter = getAccount(sourceAccount.getId());
        Account targetAccountAfter = getAccount(targetAccount.getId());
        // Balance is correct.
        assertEquals(sourceAccount.getBalance(), sourceAccountAfter.getBalance());
        assertEquals(targetAccount.getBalance().add(transferOtherToRev.getAmount()), targetAccountAfter.getBalance());
    }

    @Test
    public void transferRevToRevNotEnoughMoney() {

        final Account sourceAccount = postAccount(new Account("Account3",
                new BigDecimal("0.50"), CurrencyType.RON,
                AccountState.ACTIVE, AccountType.REVOLUT));
        final Account targetAccount = postAccount(new Account("Account2",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));

        final Transfer transferRevToRevNotEnoughMoney = new Transfer(
                sourceAccount.getId(),
                targetAccount.getId(),
                new BigDecimal("20.00"),
                CurrencyType.EUR,
                "Lunch");
        final Transaction response = postTransfer(transferRevToRevNotEnoughMoney);

        // Transaction is correct.
        assertEquals(TransactionState.DECLINED, response.getState());

        Account sourceAccountAfter = getAccount(sourceAccount.getId());
        Account targetAccountAfter = getAccount(targetAccount.getId());
        // Balance is correct.
        assertEquals(sourceAccount.getBalance(), sourceAccountAfter.getBalance());
        assertEquals(targetAccount.getBalance(), targetAccountAfter.getBalance());
    }

    @Test
    public void transferToNonExistentAccount() {

        final Account sourceAccount = postAccount(new Account("Account2",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));

        final Transfer transferToNonExistentAccount = new Transfer(
                sourceAccount.getId(),
                NON_EXISTENT_ACCOUNT_UUID,
                new BigDecimal("40.00"),
                CurrencyType.EUR,
                "Shopping");

        final Transaction response = postTransfer(transferToNonExistentAccount);

        // Transaction is correct.
        assertEquals(TransactionState.FAILED, response.getState());

        Account sourceAccountAfter = getAccount(sourceAccount.getId());
        // Balance is correct.
        assertEquals(sourceAccount.getBalance(), sourceAccountAfter.getBalance());
    }

    @Test
    public void transferFromNonExistentAccount() {

        final Account targetAccount = postAccount(new Account("Account2",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));

        final Transfer transferFromNonExistentAccount = new Transfer(
                NON_EXISTENT_ACCOUNT_UUID,
                targetAccount.getId(),
                new BigDecimal("40.00"),
                CurrencyType.EUR,
                "Shopping");
        final Transaction response = postTransfer(transferFromNonExistentAccount);

        // Transaction is correct.
        assertEquals(TransactionState.FAILED, response.getState());

        Account targetAccountAfter = getAccount(targetAccount.getId());
        // Balance is correct.
        assertEquals(targetAccount.getBalance(), targetAccountAfter.getBalance());
    }

    @Test
    public void transferToInactiveAccount() {

        final Account sourceAccount = postAccount(new Account("Account2",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));

        final Account targetAccount = postAccount(new Account("Account8",
                BigDecimal.ONE, CurrencyType.EUR,
                AccountState.INACTIVE, AccountType.REVOLUT));

        final Transfer transferToInactiveAccount = new Transfer(
                sourceAccount.getId(),
                targetAccount.getId(),
                new BigDecimal("40.00"),
                CurrencyType.EUR,
                "Shopping");

        final Transaction response = postTransfer(transferToInactiveAccount);

        // Transaction is correct.
        assertEquals(TransactionState.FAILED, response.getState());

        Account sourceAccountAfter = getAccount(sourceAccount.getId());
        // Balance is correct.
        assertEquals(sourceAccount.getBalance(), sourceAccountAfter.getBalance());
    }

    @Test
    public void transferFromInactiveAccount() {

        final Account sourceAccount = postAccount(new Account("Account8",
                BigDecimal.ONE, CurrencyType.EUR,
                AccountState.INACTIVE, AccountType.REVOLUT));
        final Account targetAccount = postAccount(new Account("Account2",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));

        final Transfer transferFromInactiveAccount = new Transfer(
                sourceAccount.getId(),
                targetAccount.getId(),
                new BigDecimal("40.00"),
                CurrencyType.EUR,
                "Shopping");

        final Transaction response = postTransfer(transferFromInactiveAccount);

        // Transaction is correct.
        assertEquals(TransactionState.FAILED, response.getState());

        Account targetAccountAfter = getAccount(targetAccount.getId());
        // Balance is correct.
        assertEquals(targetAccount.getBalance(), targetAccountAfter.getBalance());
    }

    @Test
    public void concurrentTransfer() throws InterruptedException {

        final Account sourceAccount = postAccount(new Account("Account2",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));
        final Account targetAccount = postAccount(new Account("Account1",
                new BigDecimal("100.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT));
        BigDecimal transferAmount = BigDecimal.TEN;
        int numberOfTransfers = 10;

        // Execute multiple times transfer from same accounts.
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfTransfers);
        List<Callable<Transaction>> tasks = new ArrayList<>();
        for (int i = 0; i < numberOfTransfers; i++) {
            tasks.add(() -> {
                Transfer transfer = new Transfer(
                        sourceAccount.getId(),
                        targetAccount.getId(),
                        transferAmount,
                        CurrencyType.EUR,
                        "Shopping");
                return postTransfer(transfer);
            });
        }
        executorService.invokeAll(tasks, 60, TimeUnit.SECONDS);
        executorService.shutdown();

        Account sourceAccountAfter = getAccount(sourceAccount.getId());
        Account targetAccountAfter = getAccount(targetAccount.getId());
        // Check that the amount of money has been withdrawn from the source account.
        assertEquals(sourceAccount.getBalance().subtract(transferAmount.multiply(BigDecimal.valueOf(numberOfTransfers))), sourceAccountAfter.getBalance());
        // Check that the amount of money has been added to the target account.
        assertEquals(targetAccount.getBalance().add(transferAmount.multiply(BigDecimal.valueOf(numberOfTransfers))), targetAccountAfter.getBalance());
    }

    private Account postAccount(Account account) {
        return RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/accounts")
                .request().post(Entity.json(account))
                .readEntity(Account.class);
    }

    private Transaction postTransfer(Transfer transfer) {
        return RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/transfers")
                .request().post(Entity.json(transfer))
                .readEntity(Transaction.class);
    }

    private Account getAccount(UUID id) {
        return RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/accounts/" + id.toString())
                .request()
                .get(Account.class);
    }

    private List<Transaction> getTransactions() {
        return RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/api/transactions/")
                .request()
                .get(Response.class)
                .readEntity(new GenericType<List<Transaction>>() {
                });
    }
}
