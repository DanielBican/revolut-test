package db.revolut.test.db;

import db.revolut.test.api.Account;
import db.revolut.test.core.AccountState;
import db.revolut.test.core.AccountType;
import db.revolut.test.core.CurrencyType;
import io.dropwizard.testing.junit5.DAOTestExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(DropwizardExtensionsSupport.class)
public class AccountDAOTest {

    public final DAOTestExtension daoTestRule = DAOTestExtension.newBuilder()
            .addEntityClass(Account.class)
            .build();

    private AccountDAO accountDAO;

    @BeforeEach
    public void setUp() {
        accountDAO = new AccountDAO(daoTestRule.getSessionFactory());
    }

    @Test
    @Disabled
    public void testCreateAccount() {
        final Account account = daoTestRule.inTransaction(() -> accountDAO.save(new Account("Account1",
                new BigDecimal("200.00"), CurrencyType.EUR,
                AccountState.ACTIVE, AccountType.REVOLUT)));
        assertNotNull(account.getId());
        assertEquals(account.getBalance(), new BigDecimal("200.00"));
        assertEquals(account.getType(), AccountType.REVOLUT);
    }

    @Test
    @Disabled
    public void testHandlesNullName() {
        assertThrows(ConstraintViolationException.class, () ->
                daoTestRule.inTransaction(() -> accountDAO.save(new Account(null,
                        new BigDecimal("200.00"), CurrencyType.EUR,
                        AccountState.ACTIVE, AccountType.REVOLUT))));
    }
}
